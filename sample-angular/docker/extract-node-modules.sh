#!/bin/sh
if [ -z "$1" ]; then
    echo "please specify extract path"
    exit 1
else
    tar --zstd -xf /usr/src/app/node_modules.tar.zst -C "$1"
fi
